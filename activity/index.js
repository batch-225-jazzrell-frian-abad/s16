// Activity

/*
    1. Debug the following code to return the correct value and mimic the output.
*/

    let num1 = 25;
    let num2 = 5;
    console.log("The result of num1 + num2 should be 30.");
    console.log("Actual Result:");
    console.log(num1 + num2);

    let num3 = 156;
    let num4 = 44;
    console.log("The result of num3 + num4 should be 200.");
    console.log("Actual Result:");
    console.log(num3 + num4);

    let num5 = 17;
    let num6 = 10;
    console.log("The result of num5 - num6 should be 7.");
    console.log("Actual Result:");
    console.log(num5-num6);


/*
    2. Given the values below, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.

*/
    let minutesHour = 60;
    let hoursDay = 24;
    let daysWeek = 7;
    let weeksMonth = 4;
    let monthsYear = 12;
    let daysYear = 365;

    let resultMinutes = minutesHour * hoursDay * daysYear;
    console.log("Total number of minutes in a year: " + resultMinutes);


/*
    3. Given the values below, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.
*/
    let tempCelsius = 132;

    let resultFahrenheit = (tempCelsius * (9/5) + 32);
    console.log("Temperature in Fahrenheit: " + resultFahrenheit);


        // Comparison Operator

        // Equality Operator (==)

    /*
        - Checks whether the operands are equal / have the same content.
        - Note  = is for assignment operator
        - Note == is for equality operator
        - Attempts to Convert and Compare operands of different data types.
        - True == 1 and False == 0


    */


        let juan = "juan";

        console.log(1 == 1); // True
        console.log(1 == 2); // False
        console.log(1 == '1'); // True
        console.log(0 == false); // True
        //Compares two strings that are the same 
        console.log('juan' == 'juan'); // True
        //Compares a string with the variable "juan" declared above 
        console.log('juan' == juan); // True


        // Inequality Operator (!=)

        /* 
            - Checks whether the operands are not equal/have different content
            - Attempts to CONVERT AND COMPARE operands of different data types
        */

        console.log(1 != 1); // False
        console.log(1 != 2); // True
        console.log(1 != '1'); // False
        console.log(0 != false); // False
        console.log('juan' != 'juan'); // False
        console.log('juan' != juan); // False


        // Strict Equality Operator (===)

        /* 
            - Checks whether the operands are equal/have the same content
            - Also COMPARES the data types of 2 values
            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
            - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
            - Strict equality operators are better to use in most cases to ensure that data types provided are correct
        */




        console.log(1 === 1); // True
        console.log(1 === 2); // False
        console.log(1 === '1'); // False
        console.log(0 === false); // False
        console.log('juan' === 'juan'); // True
        console.log('juan' === juan); // True


        // Relational Operators

        //Some comparison operators check whether one value is greater or less than to the other value.

        let a = 50;
        let b = 65;

        //GT or Greater Than operator ( > )
        let isGreaterThan = a > b; // 50 > 65 // False
        //LT or Less Than operator ( < ) 
        let isLessThan = a < b; // 50 < 65 // True
        //GTE or Greater Than Or Equal operator ( >= ) 
        let isGTorEqual = a >= b; // 50 >= 65 // False
        //LTE or Less Than Or Equal operator ( <= ) 
        let isLTorEqual = a <= b; // 50 <= 65 // True

        //Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.
        console.log(isGreaterThan);
        console.log(isLessThan);
        console.log(isGTorEqual);
        console.log(isLTorEqual);


        // Logical Operators (&&, ||, !)

        let isLegalAge = true;
        let isRegistered = false;

        // Logical "AND" Operator (&& - Double Ampersand - "And" Operator)
        // Return true if all operands are true
        // Note True = 1 false = 0

        // 1 && 1 = true
        // 1 && 0 = false
        // 0 && 1 = false
        // 0 && 0 = false


        let allRequirements = isLegalAge && isRegistered;
        console.log("Result of logical AND operator " + allRequirements);

        // Logical "OR" Operator (|| - Double Pipe)
        // Returns true if one of the operands are true 

        // 1 || 1 = true
        // 1 || 0 = true
        // 0 || 1 = true
        // 0 || 0 = false

        let someRequirementsMet = isLegalAge || isRegistered; 
        console.log("Result of logical OR Operator: " + someRequirementsMet);

        // Logical "Not" Operator (! - Exclamation Point)
        // Returns the opposite value 
        let someRequirementsNotMet = !isRegistered;
        console.log("Result of logical NOT Operator: " + someRequirementsNotMet); // True

        // Legal - True isRegistered - false

        let Requirements = !isLegalAge && !isRegistered; 
        console.log(Requirements);


/*
        4a. Given the values below, identify if the values of the following variable are divisible by 8.
           -Use a modulo operator to identify the divisibility of the number to 8.
           -Save the result of the operation in an appropriately named variable.
           -Log the value of the remainder in the console.
           -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy8
           -Log a message in the console if num7 is divisible by 8.
           -Log the value of isDivisibleBy8 in the console.

*/
        let num7 = 165;
        //Log the value of the remainder in the console.
        console.log("Is num7 divisible by 8?");
        //Log the value of isDivisibleBy8 in the console.
        let isDivisibleBy8 = 165 % 8;
        console.log("The remainder of " + num7 + " divided by 8 is:" +isDivisibleBy8);
        console.log(0 === isDivisibleBy8);




/*
        4b. Given the values below, identify if the values of the following variable are divisible by 4.
           -Use a modulo operator to identify the divisibility of the number to 4.
           -Save the result of the operation in an appropriately named variable.
           -Log the value of the remainder in the console.
           -Using the strict equality operator, check if the remainder is equal to 0. Save the returned value of the comparison in a variable called isDivisibleBy4
           -Log a message in the console if num8 is divisible by 4.
           -Log the value of isDivisibleBy4 in the console.

*/
        let num8 = 348;
        //Log the value of the remainder in the console.
        console.log("Is num8 divisible by 4?");
        //Log the value of isDivisibleBy4 in the console.
        let isDivisibleBy4 = 348 % 4;
        console.log("The remainder of " + num8 + " divided by 4 is:" +isDivisibleBy4);
        console.log(0 === isDivisibleBy4);
