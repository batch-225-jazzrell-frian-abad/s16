// Assignment Operators

// Basic Assignment Operator (=)
        // The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let	assignmentNumber = 8;
console.log(assignmentNumber);

// Addition Assignment Operator

let totalNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + totalNumber);

// Arithmetic Operator (+, -, *, /, %)

let x = 2; 
let y = 5;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of subtraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + product);

let quotient = y / x;
console.log("Result of quotient operator: " + quotient);

let	remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// Mini Activity:


// Multiple Operators and Parenthesis (MDAS and PEMDAS)

/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6*/

let	mDas = 1 + 2 - 3 * 4 / 5;

// let	mDas2 = 6 / 2 * (1 + 2);
console.log("Result of mDas operation: " + mDas);
// console.log("Result of mDas operation: " + mDas2);

let	pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

